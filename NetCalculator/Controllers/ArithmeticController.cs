﻿using NetCalculator.Infrastructure;
using System;
using System.Web.Http;

namespace NetCalculator
{
    public class ArithmeticController : ApiController
    {
        public IHttpActionResult Evaluate(string expression)
        {
            var a = new ArithmeticParser();
            try
            {
                return Ok(expression + " = " + a.Parse(expression));
            }
            catch (Exception e)
            {
                return Ok("Error: " + e.Message);
            }
        }
    }
}