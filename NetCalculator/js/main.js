﻿$(function () {
    $("#expressionInput").keydown(function (e) {
        if (e.keyCode == 13) {
            $.post("/api/arithmetic/evaluate?expression=" + encodeURIComponent($("#expressionInput").val()), function (data) {
                $("#logBox").append("<p>" + data + "</p>");
            }, "json");
        }
    });
});