﻿using NetCalculator.MathLibrary;
using NetCalculator.MathLibrary.Operators;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace NetCalculator.Infrastructure
{
    /// <summary>
    /// Represents arithmetic expression parser
    /// </summary>
    public class ArithmeticParser
    {
        /// <summary>
        /// Used to access to operators for priority checking
        /// </summary>
        protected Stack<IMathOperator> operatorsStack;
        
        /// <summary>
        /// Stores operands, function parameters and partial result
        /// </summary>
        protected Stack<double> operandsStack;

        /// <summary>
        /// Regex to search function in expression
        /// </summary>
        private Regex functionsRegex;

        /// <summary>
        /// Regex to search constants in expression
        /// </summary>
        private Regex constantsRegex;

        /// <summary>
        /// Regex to get number from expression
        /// </summary>
        private static Regex numberRegex = new Regex(@"^[\+|\-]?\d*[\.]?\d*", RegexOptions.Compiled);

        /// <summary>
        /// Contains set of supported operators
        /// </summary>
        private static Dictionary<string, IMathOperator> operatorsDict = new Dictionary<string, IMathOperator>()
        {
            { "+", new PlusOperator() },
            { "-", new MinusOperator() },
            { "*", new MultiplicationOperator() },
            { "/", new DivisionOperator() }
        };

        /// <summary>
        /// Priority of the last operator in operators stack
        /// </summary>
        protected Priority LastOperatorPriority
        {
            get
            {
                return operatorsStack.Count != 0 ? operatorsStack.Peek().Priority : Priority.Low;
            }
        }

        /// <summary>
        /// Contains pairs [const name - value]
        /// </summary>
        public static Dictionary<string, double> Constants { get; set; } = new Dictionary<string, double>();

        public ArithmeticParser()
        {
            operandsStack = new Stack<double>();
            operatorsStack = new Stack<IMathOperator>();
            RebuildPatterns();
        }

        /// <summary>
        /// Parses and evaluates expression
        /// </summary>
        /// <param name="expression">arithmetic expression</param>
        /// <returns>result of expression evaluation </returns>
        public double Parse(string expression)
        {
            expression = expression.Replace(" ", "");
            Evaluate(expression, Priority.Low);
            if (operandsStack.Count > 0)
                return operandsStack.Pop();
            return 0;
        }

        /// <summary>
        /// Builds regex pattern from keys of source dictonary
        /// </summary>
        /// <returns>regex pattern string representation</returns>
        protected string BuildPattern<TKey, TVal>(IDictionary<TKey, TVal> source)
        {
            var patternBuilder = new StringBuilder("^");
            var i = 0;
            foreach (var item in source)
            {
                patternBuilder.AppendFormat("({0})", item.Key);
                i++;
                if (i < source.Count)
                    patternBuilder.Append("|");
            }
            return patternBuilder.ToString();
        }

        /// <summary>
        /// Rebuilds functions and contatns searching regex
        /// </summary>
        protected void RebuildPatterns()
        {
            functionsRegex = new Regex(BuildPattern(MathFuncFactory.RealizationTypes));
            constantsRegex = new Regex(BuildPattern(Constants));
        }

        /// <summary>
        /// Matches number regex with s
        /// </summary>
        /// <param name="s">number string</param>
        /// <returns>index of last char in matched number substring</returns>
        protected int GetNumber(string s)
        {
            var m = numberRegex.Match(s);
            if (m.Success)
            {
                var result = 0d;
                if (double.TryParse(m.Value.Replace(".", ","), out result))
                {
                    operandsStack.Push(result);
                    return m.Length - 1;
                }
            }
            throw new FormatException("Float number is not recognized");
        }

        /// <summary>
        /// Matches expression with function names
        /// </summary>
        /// <param name="expression">math expression</param>
        /// <returns>function name found in expression</returns>
        protected string FindFunctionName(string expression)
        {
            return GetNameFromRegex(expression, functionsRegex);
        }

        /// <summary>
        /// Matches expression with constant names 
        /// </summary>
        /// <param name="expression">math expression</param>
        /// <returns>name of found constant</returns>
        protected string FindConstantName(string expression)
        {
            return GetNameFromRegex(expression, constantsRegex);
        }

        protected string GetNameFromRegex(string expression, Regex pattern)
        {
            var m = pattern.Match(expression);
            if (m.Success)
                return m.Value;
            return string.Empty;
        }

        /// <summary>
        /// Gets count of function arguments
        /// </summary>
        /// <param name="expression">inner function expression</param>
        /// <returns>count of function arguments</returns>
        protected int GetArgumentsCount(string expression)
        {
            if (string.IsNullOrEmpty(expression))
                throw new ArgumentException("Empty function argument");
            var argsCount = 1;
            for (int i = 0; i < expression.Length; i++)
            {
                if (char.IsLetter(expression[i]))
                {
                    var substr = expression.Substring(i);
                    var name = FindConstantName(substr);
                    i += name.Length;
                    name = FindFunctionName(substr);
                    if (!string.IsNullOrEmpty(name))
                    {
                        var j = FindClosedBracket(substr);
                        i += j;
                    }
                }
                else if (expression[i] == ',')
                    argsCount++;
            }
            return argsCount;
        }

        /// <summary>
        /// Evaluate expression as function using function name
        /// </summary>
        /// <param name="name">name of function</param>
        /// <returns>index in expression where evaluation has finished</returns>
        protected int EvaluateFunction(string name, string expression)
        {
            // get function
            var func = MathFuncFactory.GetMathFunc(name);
            // check out definition
            var m = new Regex(string.Format(@"^{0}(\()", name)).Match(expression);
            if (m.Success)
            {
                var closedBracketIndex = FindClosedBracket(expression);

                // check params count
                var argumentsExpr = expression.Substring(m.Groups[1].Index + 1, closedBracketIndex - m.Groups[1].Index - 1);
                var argsCount = GetArgumentsCount(argumentsExpr);
                if (func.VariousArgs && argsCount > func.ExpectedArgsCount)
                    func.ExpectedArgsCount = argsCount;
                else if (argsCount != func.ExpectedArgsCount)
                    throw new ArgumentException(string.Format("Incorrect number of parameters for function {0}() ",name));

                // evaluate params
                Evaluate(expression.Substring(m.Groups[1].Index + 1, closedBracketIndex - m.Groups[1].Index - 1), Priority.High);

                // evaluate function
                operandsStack.Push(func.Evaluate(operandsStack));
                return closedBracketIndex;
            }
            throw new ArgumentException("Incorrect definition for function " + name);
        }

        /// <summary>
        /// Evaluate expression as constant
        /// </summary>
        /// <returns>index in expression where evaluation has finished</returns>
        protected int EvaluateConstant(string expression)
        {
            var constName = FindConstantName(expression);
            if (string.IsNullOrEmpty(constName))
                throw new ArgumentException("Unknown constant");
            operandsStack.Push(Constants[constName]);
            return constName.Length - 1;
        }

        /// <summary>
        /// Checks brackets pairs to find closed bracket for first opened one
        /// </summary>
        /// <param name="expression">math expression</param>
        /// <returns>position of closed bracket
        /// that corresponds to first opened bracket in expression</returns>
        protected int FindClosedBracket(string expression)
        {
            int i, count = 0;
            for (i = 0; i < expression.Length; i++)
            {
                if (expression[i] == '(')
                {
                    count++;
                }
                else if (expression[i] == ')')
                {
                    count--;
                    if (count == 0)
                        return i;
                    if (count < 0)
                        throw new ArgumentException("Unexpected closed bracket ) found");
                }
            }
            throw new ArgumentException("Unclosed bracket found");
        }

        /// <summary>
        /// Evaluates expression.
        ///  It is used for recursive evaluation of expression parts.
        /// </summary>
        /// <param name="expression">arithmetic expression</param>
        /// <param name="contextPriority">priority of expression.
        /// It is used to increase priority of brackets expression</param>
        /// <returns>index in end of expression</returns>
        protected int Evaluate(string expression, Priority contextPriority)
        {
            int i;
            for (i = 0; i < expression.Length; i++)
            {
                if (expression[i] == ',')
                {
                    continue;
                }
                if (expression[i] == '(')
                {
                    // find ')'
                    var closedBracketIndex = FindClosedBracket(expression.Substring(i));
                    // evaluate brackets
                    var startInnerIndex = i + 1;
                    // set expression priority to High
                    // for evaluating brackets despite of last operator priority
                    Evaluate(expression.Substring(startInnerIndex, closedBracketIndex - startInnerIndex), Priority.High);
                    if (LastOperatorPriority == Priority.High)
                        return closedBracketIndex + 1;
                    i += closedBracketIndex;
                }
                // If number, put number into stack
                else if (char.IsNumber(expression[i]))
                {
                    var innerIndex = GetNumber(expression.Substring(i));
                    if (contextPriority < LastOperatorPriority)
                        return innerIndex + 1;
                    i += innerIndex;
                }
                // If operator
                else if (operatorsDict.ContainsKey(expression[i].ToString()))
                {
                    var oper = operatorsDict[expression[i].ToString()];
                    if (i == 0 && oper is MinusOperator && char.IsNumber(expression[i + 1]))
                    {
                        // to cover case "-Number" in a begin of scope
                        var innerIndex = GetNumber(expression.Substring(i));
                        i += innerIndex;
                        continue;
                    }
                    operatorsStack.Push(oper);
                    var j = Evaluate(expression.Substring(i + 1), Priority.Low);   // evaluate next operand
                    var res = operatorsStack.Pop().Evaluate(operandsStack);
                    operandsStack.Push(res);
                    i += j;
                }
                // if function or constant
                else if (char.IsLetter(expression[i]))
                {
                    var substr = expression.Substring(i);
                    // find function to evaluate
                    var funcName = FindFunctionName(substr);
                    if (!string.IsNullOrEmpty(funcName))
                    {
                        i += EvaluateFunction(funcName, substr);
                    }
                    else
                    {
                        i += EvaluateConstant(substr);
                        if (contextPriority < LastOperatorPriority)
                            return i + 1; // exit to outer scope
                    }
                }
                // unexpected symbol
                else throw new ArgumentException("Unexpected symbol at " + i);
            }
            // end of expression scope (brackets, function etc.)
            return i;
        }
    }
}