﻿using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using NetCalculator.MathLibrary;
using NetCalculator.Infrastructure;

namespace NetCalculator
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            
            // Load constants and function difinitions from config
            var consts = ConfigurationManager.GetSection("constants") as Hashtable;
            ArithmeticParser.Constants = consts
                .Cast<DictionaryEntry>()
                .ToDictionary(k=>k.Key.ToString(), v => double.Parse(v.Value.ToString()));

            var funcs = ConfigurationManager.GetSection("functions") as Hashtable;
            MathFuncFactory.RealizationTypes = funcs
                .Cast<DictionaryEntry>()
                .ToDictionary(k=>k.Key.ToString(), v=>v.Value.ToString());
        }
    }
}
