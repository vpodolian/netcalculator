﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetCalculator.MathLibrary;
using NetCalculator.MathLibrary.Functions;

namespace NetCalculator.Tests
{
    [TestClass]
    public class MathFuncFactoryTests
    {
        [TestInitialize]
        public void TestInit()
        {
            MathFuncFactory.RealizationTypes.Clear();
        }

        [TestMethod]
        public void GetMathFunc_creates_instance_by_type_name()
        {
            var funcName = "abs";
            var funcTypeName = "NetCalculator.MathLibrary.Functions.AbsFunc";
            MathFuncFactory.RealizationTypes.Add(funcName, funcTypeName);
            var mathFunction = MathFuncFactory.GetMathFunc(funcName);
            Assert.AreSame(mathFunction.GetType(), typeof(AbsFunc));
        }

        [TestMethod]
        public void GetMathFunc_returns_instance_from_pool()
        {
            var funcName = "abs";
            var funcTypeName = "NetCalculator.MathLibrary.Functions.AbsFunc";
            MathFuncFactory.RealizationTypes.Add(funcName, funcTypeName);
            var mathFunction = MathFuncFactory.GetMathFunc(funcName);

            mathFunction = null;
            MathFuncFactory.RealizationTypes.Clear(); // Clear dictionary to be imposible create instance by type name

            mathFunction = MathFuncFactory.GetMathFunc(funcName);
            Assert.AreSame(mathFunction.GetType(), typeof(AbsFunc));
        }
    }
}
