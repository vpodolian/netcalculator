﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetCalculator.MathLibrary.Operators;
using System.Collections.Generic;

namespace NetCalculator.Tests.Operators
{
    [TestClass]
    public class PlusOperatorTests
    {
        [TestMethod]
        public void PlusOperator_adds_two_operands()
        {
            double a = 2, b = 10; 
            var s = new Stack<double>();
            s.Push(a);
            s.Push(b);
            Assert.AreEqual(a + b, new PlusOperator().Evaluate(s));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void PlusOperator_throws_exception_if_operands_stack_doesnt_have_2_items()
        {
            var s = new Stack<double>();
            new PlusOperator().Evaluate(s);
        }
    }
}
