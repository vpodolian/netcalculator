﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetCalculator.Infrastructure;
using NetCalculator.MathLibrary;
using System;
using System.Text.RegularExpressions;

namespace NetCalculator.Tests
{
    [TestClass]
    public class ArithmeticParserTests : ArithmeticParser
    {
        [TestMethod]
        public void GetNumber_returns_number_from_string()
        {
            var actual = "10.1daa";
            var expected = 10.1d;
            GetNumber(actual);
            Assert.AreEqual(expected, operandsStack.Pop());
        }

        [TestMethod]
        public void GetNumber_returns_index_after_last_number_symbol()
        {
            var actual = "20.43 +";
            var expected = 4;
            Assert.AreEqual(expected, GetNumber(actual));
        }

        [TestMethod]
        public void GetNumber_parses_negative_number_correctly()
        {
            var actual = "-20.13";
            var expected = -20.13d;
            GetNumber(actual);
            Assert.AreEqual(expected, operandsStack.Pop());
        }

        [TestMethod]
        public void Evaluate_simple_expression()
        {
            var expression = "1 + 20.1";
            Assert.AreEqual(21.1, Parse(expression));
        }

        [TestMethod]
        public void BuildPattern_returns_correct_regex_pattern()
        {
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("abs", "test");
            MathFuncFactory.RealizationTypes.Add("max", "test");
            Assert.AreEqual("^(abs)|(max)", BuildPattern(MathFuncFactory.RealizationTypes));
        }

        [TestMethod]
        public void FindFunctionName_returns_function_name_found_in_expr_begin()
        {
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("abs", "test");
            MathFuncFactory.RealizationTypes.Add("max", "test");
            RebuildPatterns();
            Assert.AreEqual("abs", FindFunctionName("abs(1)"));
        }

        [TestMethod]
        public void EvaluateFunction_evluates_function_with_1_param()
        {
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("abs", "NetCalculator.MathLibrary.Functions.AbsFunc");
            RebuildPatterns();
            EvaluateFunction("abs", "abs(-1+abs(-10)*2)");
            Assert.AreEqual(19, operandsStack.Pop());
        }

        [TestMethod]
        public void EvaluateFuncton_evaluates_sqrt()
        {
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("sqrt", "NetCalculator.MathLibrary.Functions.SqrtFunc");
            RebuildPatterns();
            EvaluateFunction("sqrt", "sqrt(16)");
            Assert.AreEqual(4, operandsStack.Pop());
        }

        [TestMethod]
        public void EvaluateFuncton_evaluates_pow()
        {
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("pow", "NetCalculator.MathLibrary.Functions.PowFunc");
            RebuildPatterns();
            EvaluateFunction("pow", "pow(2,3)");
            Assert.AreEqual(8, operandsStack.Pop());
        }

        [TestMethod]
        public void EvaluateFuncton_evaluates_max()
        {
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("max", "NetCalculator.MathLibrary.Functions.MaxFunc");
            RebuildPatterns();
            EvaluateFunction("max", "max(2,3,1)");
            Assert.AreEqual(3, operandsStack.Pop());
        }

        [TestMethod]
        public void EvaluateFuncton_evaluates_min()
        {
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("min", "NetCalculator.MathLibrary.Functions.MinFunc");
            RebuildPatterns();
            EvaluateFunction("min", "min(2,3,1)");
            Assert.AreEqual(1, operandsStack.Pop());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void EvaluateFunction_throws_exception_when_lack_of_function_params()
        {
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("min", "NetCalculator.MathLibrary.Functions.MinFunc");
            MathFuncFactory.RealizationTypes.Add("pow", "NetCalculator.MathLibrary.Functions.PowFunc");
            RebuildPatterns();
            EvaluateFunction("min", "min(2)");
            EvaluateFunction("pow", "pow(1,3,2)");
        }

        [TestMethod]
        public void EvaluateFunction_evaluates_with_inner_function()
        {
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("min", "NetCalculator.MathLibrary.Functions.MinFunc");
            MathFuncFactory.RealizationTypes.Add("pow", "NetCalculator.MathLibrary.Functions.PowFunc");
            RebuildPatterns();
            EvaluateFunction("pow", "pow(2,min(2,3,1))");
            Assert.AreEqual(2, operandsStack.Pop());
        }

        [TestMethod]
        public void FindClosedBracket_returns_index_of_closed_bracket_in_expression()
        {
            Assert.AreEqual(8, FindClosedBracket("((1+2)*2)*(2+10)"));
        }

        [TestMethod]
        public void Evaluate_multiple_brackets_correctly()
        {
            Assert.AreEqual(20, Parse("2*(3+2)+(1+4)*2"));
        }

        [TestMethod]
        public void Evaluate_expression_with_constant()
        {
            Constants.Clear();
            Constants.Add("PI", 3.14);
            RebuildPatterns();
            Assert.AreEqual(16.28, Parse("2*PI+10"));
        }

        [TestMethod]
        public void Evaluate_function_with_constant()
        {
            Constants.Clear();
            Constants.Add("a", 5);
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("pow", "NetCalculator.MathLibrary.Functions.PowFunc");
            RebuildPatterns();

            Assert.AreEqual(25, Parse("pow(a,2)"));
        }

        [TestMethod]
        public void Evaluate_function_with_constant_n_brackets()
        {
            Constants.Clear();
            Constants.Add("a", 2);
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("pow", "NetCalculator.MathLibrary.Functions.PowFunc");
            RebuildPatterns();

            Assert.AreEqual(64, Parse("pow(a,2*(1+2))"));
        }

        [TestMethod]
        public void Evaluate_brackets_with_constant()
        {
            Constants.Clear();
            Constants.Add("a", 2);
            RebuildPatterns();
            Assert.AreEqual(13, Parse("1+2*(a*2+2)"));
        }

        [TestMethod]
        public void Evaluate_function_n_operator_with_high_priority()
        {
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("pow", "NetCalculator.MathLibrary.Functions.PowFunc");
            RebuildPatterns();

            Assert.AreEqual(11, Parse("1+2*3+pow(2,2)"));
        }

        [TestMethod]
        public void Evaluate_multiplication_in_brackets()
        {
            Assert.AreEqual(10, Parse("2*(1*2+3)"));
        }

        [TestMethod]
        public void Evaluate_unary_minus_with_function()
        {
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("min", "NetCalculator.MathLibrary.Functions.MinFunc");
            MathFuncFactory.RealizationTypes.Add("sqrt", "NetCalculator.MathLibrary.Functions.SqrtFunc");
            RebuildPatterns();

            Assert.AreEqual(-190, Parse("-min(200, 200-sqrt(100))"));
        }

        [TestMethod]
        public void ParseArgumentsCount_counts_function_params()
        {
            MathFuncFactory.RealizationTypes.Clear();
            MathFuncFactory.RealizationTypes.Add("pow", "NetCalculator.MathLibrary.Functions.PowFunc");
            RebuildPatterns();
            Assert.AreEqual(3, GetArgumentsCount("1,2,pow(2,1)"));
        }
    }
}
