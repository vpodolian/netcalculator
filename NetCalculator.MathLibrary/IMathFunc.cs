﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace NetCalculator.MathLibrary
{
    /// <summary>
    /// Represents a mathematical funtion.
    /// It should be impemented by custom functions
    /// </summary>
    public interface IMathFunc: IEvaluable
    {
        /// <summary>
        /// Minimal count of function arguments
        /// </summary>
        int ExpectedArgsCount { get; set; }

        /// <summary>
        /// Indicates if function has various number of arguments
        /// </summary>
        bool VariousArgs { get; }

    }
}
