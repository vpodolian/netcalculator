﻿using System.Collections.Generic;

namespace NetCalculator.MathLibrary
{
    /// <summary>
    /// Represents an math object that can be evaluated
    /// </summary>
    public interface IEvaluable
    {
        /// <summary>
        /// Evaluates by using operands stack
        /// </summary>
        /// <param name="operands">is used to get operands</param>
        double Evaluate(Stack<double> operands);
    }
}
