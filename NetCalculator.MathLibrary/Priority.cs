﻿namespace NetCalculator.MathLibrary.Operators
{
    /// <summary>
    /// Evaluation priority
    /// </summary>
    public enum Priority
    {
        Low,
        High
    }
}
