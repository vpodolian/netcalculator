﻿using System;
using System.Collections.Generic;

namespace NetCalculator.MathLibrary.Operators
{
    public class MultiplicationOperator : MathBase, IMathOperator
    {
        public Priority Priority { get { return Priority.High; } }
        public double Evaluate(Stack<double> operands)
        {
            CheckArgumentsCount(operands, 2);
            return operands.Pop() * operands.Pop();
        }
    }
}
