﻿using System.Collections.Generic;

namespace NetCalculator.MathLibrary.Operators
{
    public class DivisionOperator : MathBase, IMathOperator
    {
        public Priority Priority { get { return Priority.High; } }

        public double Evaluate(Stack<double> operands)
        {
            CheckArgumentsCount(operands, 2);
            var temp = operands.Pop();
            return operands.Pop() / temp;
        }
    }
}
