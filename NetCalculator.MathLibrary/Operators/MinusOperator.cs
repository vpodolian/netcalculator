﻿using System;
using System.Collections.Generic;

namespace NetCalculator.MathLibrary.Operators
{
    public class MinusOperator : MathBase, IMathOperator
    {
        public Priority Priority { get { return Priority.Low; } }
        public double Evaluate(Stack<double> operands)
        {
            CheckArgumentsCount(operands, 1);
            var temp = operands.Pop();
            return operands.Count > 0 ? operands.Pop() - temp : 0 - temp;
        }
    }
}
