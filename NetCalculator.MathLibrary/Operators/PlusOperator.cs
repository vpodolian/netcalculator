﻿using System.Collections.Generic;

namespace NetCalculator.MathLibrary.Operators
{
    public class PlusOperator : MathBase, IMathOperator
    {
        public Priority Priority { get { return Priority.Low; } }
        public double Evaluate(Stack<double> operands)
        {
            CheckArgumentsCount(operands, 2);
            return operands.Pop() + operands.Pop();
        }
    }
}
