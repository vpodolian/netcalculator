﻿using System;
using System.Collections.Generic;

namespace NetCalculator.MathLibrary
{
    public static class MathFuncFactory
    {
        /// <summary>
        /// Pool of function realizations for quick access
        /// </summary>
        private static Dictionary<string, IMathFunc> pool;

        /// <summary>
        /// [Function name - Ralization type name] dictionary
        /// </summary>
        private static Dictionary<string, string> realizationTypes;

        static MathFuncFactory()
        {
            pool = new Dictionary<string, IMathFunc>();
            RealizationTypes = new Dictionary<string, string>();
        }

        public static Dictionary<string, string> RealizationTypes
        {
            get { return realizationTypes; }
            set { realizationTypes = value; }
        }

        public static IMathFunc GetMathFunc(string name)
        {
            if (pool.ContainsKey(name))
                return pool[name];

            var function = (IMathFunc)Activator.CreateInstance(Type.GetType(RealizationTypes[name]));
            if (function == null)
                throw new NotImplementedException("Не найдена реализация фукнции " + name);

            pool.Add(name, function);
            return function;
        }
    }
}