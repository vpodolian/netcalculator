﻿using NetCalculator.MathLibrary.Operators;

namespace NetCalculator.MathLibrary
{
    /// <summary>
    /// This interface may be used for future extension of math operators functionality  
    /// </summary>
    public interface IMathOperator: IEvaluable
    {
        Priority Priority { get; }
    }
}
