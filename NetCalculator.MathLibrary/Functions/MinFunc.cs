﻿using System.Collections.Generic;
using System.Linq;

namespace NetCalculator.MathLibrary.Functions
{
    public class MinFunc : MathBase, IMathFunc
    {
        public override int ExpectedArgsCount { get { return 2; } }
        public bool VariousArgs { get { return true; } }
        public double Evaluate(Stack<double> operands)
        {
            CheckArgumentsCount(operands, ExpectedArgsCount);
            double[] args = new double[ExpectedArgsCount];
            double min = double.MaxValue, temp;
            for (int i = 0; i < ExpectedArgsCount; i++)
            {
                temp = operands.Pop();
                if (min > temp)
                    min = temp;
            }
            return min;
        }
    }
}
