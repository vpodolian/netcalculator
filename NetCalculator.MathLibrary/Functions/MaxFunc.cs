﻿using System;
using System.Collections.Generic;

namespace NetCalculator.MathLibrary.Functions
{
    public class MaxFunc : MathBase, IMathFunc
    {
        public override int ExpectedArgsCount { get { return 2; } }
        public bool VariousArgs { get { return true; } }
        public double Evaluate(Stack<double> operands)
        {
            CheckArgumentsCount(operands, ExpectedArgsCount);
            double[] args = new double[ExpectedArgsCount];
            double max = double.MinValue, temp;
            for (int i = 0; i < ExpectedArgsCount; i++)
            {
                temp = operands.Pop();
                if (max < temp)
                    max = temp;
            }
            return max;
        }
    }
}
