﻿using System;
using System.Collections.Generic;

namespace NetCalculator.MathLibrary.Functions
{
    public class PowFunc : MathBase, IMathFunc
    {
        public override int ExpectedArgsCount { get { return 2; } }
        public bool VariousArgs { get { return false; } }
        public double Evaluate(Stack<double> operands)
        {
            var y = operands.Pop();
            return Math.Pow(operands.Pop(), y);
        }
    }
}
