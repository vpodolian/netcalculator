﻿using System;
using System.Collections.Generic;

namespace NetCalculator.MathLibrary.Functions
{
    public class RoundFunc : MathBase, IMathFunc
    {
        public override int ExpectedArgsCount { get { return 1; } }
        public bool VariousArgs { get { return false; } }
        public double Evaluate(Stack<double> operands)
        {
            return Math.Round(operands.Pop());
        }
    }
}
