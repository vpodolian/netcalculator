﻿using System;
using System.Collections.Generic;

namespace NetCalculator.MathLibrary.Functions
{
    public class AbsFunc : MathBase, IMathFunc
    {
        public override int ExpectedArgsCount { get { return 1; } }
        public bool VariousArgs { get { return false; } }
        public double Evaluate(Stack<double> operands)
        {
            if (operands.Count < 1)
                throw new ArgumentException("Lack of operands. 1 operand needed");
            return Math.Abs(operands.Pop());
        }
    }
}
