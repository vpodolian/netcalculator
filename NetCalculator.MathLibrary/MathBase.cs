﻿using System;
using System.Collections.Generic;

namespace NetCalculator.MathLibrary
{
    /// <summary>
    /// Contains some useful members to implement custom operands and functions
    /// </summary>
    public abstract class MathBase
    {
        public virtual int ExpectedArgsCount { get; set; }
        protected void CheckArgumentsCount(Stack<double> operands, int argsCount)
        {
            if (operands.Count < argsCount)
                throw new Exception(string.Format("Lacks of operands in stack. {0} needed", argsCount));
        }
    }
}
